#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "ox_board.h"

int main(int argc, char *argv[]) {

    QGuiApplication app(argc, argv);

    OXBoard b(0);
    qmlRegisterType<OXBoard>("OXBoard", 1, 0, "OXBoard");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();

}
