import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    visible: true
    title: "tic-tac_toe"

    //3G & 3GS  = 320×480   0.66r:1
    // 4 & 4S   = 640×960   0.66r:1
    // 5C & 5S  = 640×1136  0.563:1
    // 6        = 750×1334  0.562:1
    // 6 plus   = 1080×1920 0.563:1
    //iPad  	= 768x1024	0.750:1
    //iPad Pro  = 1024x1366	0.749:1

    width: 960//1024//960//640//Screen.desktopAvailableWidth
    height: 640//768//640//960//Screen.desktopAvailableWidth

    OXGame {
        id: game
    }

    GridLayout {
        x: 0
        y: 0
        width: parent.width > parent.height ? (parent.width - game.width) / 2 : game.width
        height: parent.width > parent.height ? parent.height : (parent.height - game.height) / 2
        flow:  width > height ? GridLayout.LeftToRight : GridLayout.TopToBottom
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#5d5b59"
            Label {
                anchors.centerIn: parent
                text: "<h1>Wipe</h1>"
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    game.reset()
                }
            }
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#1e1b18"
            Label {
                anchors.centerIn: parent
                text: "<h1>Play</h1>"
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    game.play()
                }
            }
        }
    }

    GridLayout {
        x: parent.width > parent.height ? game.x + game.width : 0
        y: parent.width > parent.height ? 0 : game.y + game.width
        width: parent.width > parent.height ? (parent.width - game.width) / 2 : game.width
        height: parent.width > parent.height ? parent.height : (parent.height - game.height) / 2
        flow:  width > height ? GridLayout.LeftToRight : GridLayout.TopToBottom
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#5d5b59"
            Label {
                id: x
                anchors.centerIn: parent
                text: "<h1>X</h1>"
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    game.beX()
                }
            }
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#1e1b18"
            Label {
                anchors.centerIn: parent
                text: "<h1>O</h1>"
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    game.beO()
                }
            }
        }
    }
}
