TEMPLATE = app

QT += qml quick

qtHaveModule(multimedia): QT += multimedia

CONFIG += c++11

SOURCES += main.cpp \
    ox_board.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS



# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ox_board.h

DISTFILES += \
    OXs-Vincent/scans as png/circles contrast 10.png \
    OXs-Vincent/scans as png/circles.png \
    OXs-Vincent/scans as png/crosses contrast 10.png \
    OXs-Vincent/scans as png/crosses.png \
    OXs-Vincent/scans as png/diagonal 2 contrast 10.png \
    OXs-Vincent/scans as png/diagonal 2.png \
    OXs-Vincent/scans as png/diagonal contrast 10.png \
    OXs-Vincent/scans as png/diagonal.png \
    OXs-Vincent/scans as png/hash.png \
    OXs-Vincent/scans as png/hatch contrast 10.png \
    OXs-Vincent/scans as png/lines contrast 10.png \
    OXs-Vincent/scans as png/lines.png \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O9.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O1-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O2-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O3-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O4-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O5-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O6-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O7-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O8-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O9-625x625.xcf \
    OXs-Vincent/scans as png/circles contrast 10.png \
    OXs-Vincent/scans as png/circles.png \
    OXs-Vincent/scans as png/crosses contrast 10.png \
    OXs-Vincent/scans as png/crosses.png \
    OXs-Vincent/scans as png/diagonal 2 contrast 10.png \
    OXs-Vincent/scans as png/diagonal 2.png \
    OXs-Vincent/scans as png/diagonal contrast 10.png \
    OXs-Vincent/scans as png/diagonal.png \
    OXs-Vincent/scans as png/hash.png \
    OXs-Vincent/scans as png/hatch contrast 10.png \
    OXs-Vincent/scans as png/lines contrast 10.png \
    OXs-Vincent/scans as png/lines.png \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O layers/O9.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/O maps/O9.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O1-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O2-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O3-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O4-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O5-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O6-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O7-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O8-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Os/O9-625x625.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X layers/X9.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/X maps/X9.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X1.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X2.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X3.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X4.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X5.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X6.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X7.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X8.xcf \
    OXs-Vincent/cut sized clean as xcf/625x625/Xs/X9.xcf
