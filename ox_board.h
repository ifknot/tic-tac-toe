#ifndef OX_BOARD_H
#define OX_BOARD_H

#include <QObject>

#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>
#include <random>
#include <chrono>
#include <functional>
#include <bitset>

class OXBoard: public QObject {

    Q_OBJECT
    Q_PROPERTY(int bits READ getBits WRITE setBits NOTIFY bitsChanged)

public:

    OXBoard(QObject* parent = nullptr);

    int getBits();

    void setBits(int bits);

public slots:

    void onBitsChanged(int bits);

signals:

    void bitsChanged(int);

private:

    int bits_;

    unsigned seed_;

    static std::default_random_engine flip;

    static std::uniform_int_distribution<int> coin;

    static const int game_on_ = 0x80000000;

    static const int draw_ = 0x1FF;

    static const int your_turn_ = 0x8000;

    static const int infinity_ = 0xFF;

    static const int maxply_ = 4;

    static const std::vector<int> wins;

    bool won(int bits);

    bool drawn(int bits);

    bool game_on(int bits);

    int win_line(int bits);

    std::vector<int> o_moves(int bits);

    std::vector<int> x_moves(int bits);

    int score(int bits);

    int minimax(int bits, int ply);

    int play(int bits);

    int play_random(int bits);

    template<typename T>
    T random_swap(T a, T b) {
        return coin(flip) ?a :b;
    }

    std::string stringify(int bits);

};

#endif // OX_BOARD_H
