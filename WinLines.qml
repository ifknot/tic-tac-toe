import QtQuick 2.0
import QtMultimedia 5.8

Item {

    property int fWidth: 0
    property int fHeight: 0
    property string wImage:"images/win_lines.png"

    function line1() {
        lines.visible = true
        lines.jumpTo("line1")
        //cheerEffect.play()
    }

    function line2() {
        lines.visible = true
        lines.jumpTo("line2")
        //cheerEffect.play()
    }

    function line3() {
        lines.visible = true
        lines.jumpTo("line3")
        //cheerEffect.play()
    }

    function line4() {
        lines.visible = true
        lines.jumpTo("line4")
        //cheerEffect.play()
    }

    function line5() {
        lines.visible = true
        lines.jumpTo("line5")
        //cheerEffect.play()
    }

    function line6() {
        lines.visible = true
        lines.jumpTo("line6")
        //cheerEffect.play()
    }

    function line7() {
        lines.visible = true
        lines.jumpTo("line7")
        //cheerEffect.play()
    }

    function line8() {
        lines.visible = true
        lines.jumpTo("line8")
        //cheerEffect.play()
    }

    function reset() {
        lines.visible = false
    }

    SpriteSequence {
        id: lines
        visible: false
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        Sprite {
            name: "line1"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 0 * fWidth
        }
        Sprite {
            name: "line2"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 1 * fWidth
        }
        Sprite {
            name: "line3"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 2 * fWidth
        }
        Sprite {
            name: "line4"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 3 * fWidth
        }
        Sprite {
            name: "line5"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 4 * fWidth
        }
        Sprite {
            name: "line6"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 5 * fWidth
        }
        Sprite {
            name: "line7"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 6 * fWidth
        }
        Sprite {
            name: "line8"
            source: wImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: 7 * fWidth
        }

    }

}
