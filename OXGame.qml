import QtQuick 2.0
import QtMultimedia 5.8
import OXBoard 1.0

Item {

    Image {
        id: board
        source: "images/board.png"
        visible: false
    }

    Image {
        id:tile
        source: "images/tile.png"
        visible: false
    }

    width: parent.width > parent.height ? parent.height : parent.width
    height: width
    anchors.centerIn: parent

    property int playerX: 0x8000
    property int on: -0x80000000
    property int thinkingTime: 400
    property bool over: false

    function reset() {
        oxboard.xprior = 0;
        oxboard.xboard = 0;
        oxboard.oprior = 0;
        oxboard.oboard = 0;
        wins.reset();
        for(var i = 0; i < 9; ++i) {
            tiles.itemAt(i).reset();
        }
        over = false
        oxboard.bits = playerX
    }

    function play() {
        if(!over) {
            oxboard.bits |= on
        }
    }

    function beX() {
        if(!over) {
            oxboard.bits |= playerX;
        }
    }

    function beO() {
        if(!over) {
            oxboard.bits &= (~playerX);
        }
    }

    Timer {
        id: delayDrawO;
        interval: thinkingTime;
        repeat: false;

        property int i: 0

        onTriggered: {
            tiles.itemAt(i).drawO();
        }

    }

    Timer {
        id: delayDrawX;
        interval: thinkingTime;
        repeat: false;

        property int i: 0

        onTriggered: {
            tiles.itemAt(i).drawX();
        }

    }

    Timer {
        id: delayDrawWin;
        interval: thinkingTime;
        repeat: false

        onTriggered: {
            switch (oxboard.bits) {
            case 1:
            case 0x10000:
                wins.line1()
                break
            case 2:
            case 0x20000:
                wins.line2()
                break
            case 3:
            case 0x30000:
                wins.line3()
                break
            case 4:
            case 0x40000:
                wins.line4()
                break
            case 5:
            case 0x50000:
                wins.line5();
                break
            case 6:
            case 0x60000:
                wins.line6()
                break
            case 7:
            case 0x70000:
                wins.line7()
                break
            case 8:
            case 0x80000:
                wins.line8()
                break
            case 9:
                break
            default:
            }
        }
    }

    OXBoard {
        id: oxboard
        property int oprior: 0
        property int oboard: 0
        property int xprior: 0
        property int xboard: 0

        function binToString(b, l) {
            var s = "";
            while(l--)
              s += (b >> l ) & 1;
            return s;
        }

        function bitIndex(board) {
            var i = 0;
            var j = 1;
            while(!(board & j)) {
                j <<= 1;
                i++;
            }
            return i;
        }

        onBitsChanged: {
            if(oxboard.bits & on) {
                if (oxboard.bits & playerX) {
                    xboard = oxboard.bits & 0x1FF;
                    if (xboard & ~xprior) {
                        delayDrawX.i = bitIndex(oboard & ~oprior);
                        delayDrawX.start();
                    }
                    xprior = xboard;
                } else {
                    oboard = (oxboard.bits >> 16) & 0x1FF;
                    if (oboard & ~oprior) {
                        delayDrawO.i = bitIndex(oboard & ~oprior);
                        delayDrawO.start();
                    }
                    oprior = oboard;
                }
            }
            else {
                switch (oxboard.bits) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    over = true
                    woohoo.play()
                    break
                case 0x10000:
                case 0x20000:
                case 0x30000:
                case 0x40000:
                case 0x50000:
                case 0x60000:
                case 0x70000:
                case 0x80000:
                    over = true
                    booh.play()
                    break
                case 9:
                    over = true
                    groan.play()
                    break
                default:
                }
                delayDrawWin.start()
            }
        }

    }

    MediaPlayer {
        id: groan
        source: "sounds/groan.m4a"
    }

    MediaPlayer {
        id: woohoo
        source: "sounds/woohoo.m4a"
    }

    MediaPlayer {
        id: booh
        source: "sounds/booh.m4a"
    }

    Image {
        source: board.source
        width: parent.width
        height: parent.width
        anchors.centerIn: parent
    }

    Grid {
        id: tilegrid
        width: parent.width
        anchors.centerIn: parent
        columns: 3
        rows: 3
        Repeater {
            id: tiles
            model: 9
            OXTile {
                i: 1 << index
                width: parent.width / 3
                height: width
                fWidth: tile.width
                fHeight: tile.height
                xImage: "images/X" + Math.ceil(Math.random() * 4) + ".png"
                oImage: "images/O" + Math.ceil(Math.random() * 4) + ".png"
                ratio:  (3 * tile.width) / board.width
            }
        }
    }

    WinLines {
        id: wins
        fWidth: board.width
        fHeight: board.height
        width: parent.width
        height: board.width * (game.width / board.width)
        anchors.centerIn: parent
    }

}

