#include "ox_board.h"

    std::default_random_engine OXBoard::flip =
        std::default_random_engine();

    std::uniform_int_distribution<int> OXBoard::coin =
        std::uniform_int_distribution<int>(0,1);

    const std::vector<int> OXBoard::wins =
      { 0b0000000000000111,
        0b0000000000111000,
        0b0000000111000000,
        0b0000000100100100,
        0b0000000010010010,
        0b0000000001001001,
        0b0000000100010001,
        0b0000000001010100 };

    OXBoard::OXBoard(QObject* parent): QObject(parent), bits_(0) {
        bits_ |= your_turn_;
        connect(this, SIGNAL(bitsChanged(int)), this, SLOT(onBitsChanged(int)));
    }

    int OXBoard::getBits() {
        return bits_;
    }

    void OXBoard::setBits(int bits) {
        bits_ = bits;
        emit bitsChanged(bits);
    }

    void OXBoard::onBitsChanged(int bits) {
        if(game_on(bits) && !won(bits) && !drawn(bits)) {
            bits_ ^= your_turn_;
            if(!(bits_ & your_turn_)) {
                setBits(play(bits_));
            }
        }
        else if (game_on(bits) ) {
            setBits(win_line(bits));
        }
    }

    std::string OXBoard::stringify(int bits) {
        int n = 0;
        std::stringstream ss;
        ss << "\n";
        for(int i = 0; i < 3; ++i) {
            for(int j = 0; j < 3; ++j) {
                if(bits & (1 << n)) ss << "X";
                else if(bits & (1 << (n + 16))) ss << "O";
                else ss << "_";
                ++n;
            }
            ss << "\n";
        }
        return ss.str();
    }

    bool OXBoard::won (int bits) {
        for(auto w : wins) {
            if(((bits & w) == w) || ((bits & (w << 16)) == (w << 16))){
                return true;
            }
        }
        return false;
    }

    bool OXBoard::drawn(int bits) {
        return ((bits | (bits >> 16)) & draw_) == draw_;
    }

    bool OXBoard::game_on(int bits) {
        return bits & game_on_;
    }

    int OXBoard::win_line(int bits) {
        size_t i;
        for(i = 0; i < wins.size(); ++i) {
            if((bits & wins[i]) == wins[i]) {
                return ++i;
            }
            else if ((bits & (wins[i] << 16)) == (wins[i] << 16)) {
               return ++i << 16;
            }
        }
        return ++i;
    }

    std::vector<int> OXBoard::o_moves(int bits) {
        int i, j;
        std::vector<int> M;
        for(i = 1, j = 0; j < 9; i <<= 1, ++j) {
            if(~(bits | (bits >> 16)) & i) M.push_back(i << 16);
        }
        return M;
    }

    std::vector<int> OXBoard::x_moves(int bits) {
        int i, j;
        std::vector<int> M;
        for(i = 1, j = 0; j < 9; i <<= 1, ++j) {
            if(~(bits | (bits >> 16)) & i) M.push_back(i);
        }
        return M;
    }

    int OXBoard::score(int bits) {
        int n = 0;
        for(auto w : wins) {
            if((bits & w) == w) {
                return -infinity_; // X wins
            }
            if((bits & (w << 16)) == (w << 16)) {
                return infinity_; // O wins
            }
            // score = (my_open_win_lines - your_open_win_lines)
            if(!(bits & w)) { // X does not block a win line
                if(bits & (w << 16)) ++n; // O occupies open win line
            }
            if(!(bits & (w << 16))) { // O does not block a win line
                if(bits & w) --n; // X occupies open win line
            }
        }
        return n;
    }

    int OXBoard::minimax(int bits, int ply) {
        if(won(bits) || (ply == maxply_)) {
            return score(bits);
        }
        else {
            if(ply & 1) { //odd ply minimise X moves
                auto M = x_moves(bits);
                std::set<int> S;
                for(auto m : M) {
                    S.insert(minimax(m | bits, ply + 1));
                }
                return *S.begin();
            }
            else { // even ply maximise O moves
                auto M = o_moves(bits);
                std::set<int> S;
                for(auto m : M) {
                    S.insert(minimax(m | bits, ply + 1));
                }
                return *S.rbegin();
            }
        }
    }

    int OXBoard::play(int bits) {
        int best_move = 0;
        int best_score = -infinity_;
        auto M = o_moves(bits);
        for(auto m : M) {
             int s = minimax(m | bits, 1);
             if(s > best_score) {
                 best_score = s;
                 best_move = m;
             }
             else if (s == best_score) {
                 best_move = random_swap(best_move, m);
             }
        }
        if(!best_move) {
            return play_random(bits);
        }
        return bits | best_move;
    }

    int OXBoard::play_random(int bits) {
        auto M = o_moves(bits);
        std::shuffle (M.begin(), M.end(), std::default_random_engine{});
        bits |= (M.front());
        return bits;
    }

