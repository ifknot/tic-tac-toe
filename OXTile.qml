import QtQuick 2.0
import QtMultimedia 5.8

Item {

    property bool drawn: false
    property int fSec: 17
    property int fCount: 8
    property int fWidth: 0
    property int fHeight: 0
    property int fLast: 7
    property double ratio: 1.0
    property string xImage: ""
    property string oImage: ""
    property string xSound: "sounds/x_pencil.m4a"
    property string oSound: "sounds/o_pencil.m4a"
    property int i

    function drawX() {
        drawn = true
        x.visible = true
        xEffect.play()
        x.jumpTo("drawing")
        while(x.currentSprite != "done") {}
    }

    function drawO() {
        drawn = true
        o.visible = true
        oEffect.play()
        o.jumpTo("drawing")
        while(o.currentSprite != "done") {}
    }

    function reset() {
        drawn = false
        x.visible = false
        o.visible = false
    }

    MediaPlayer {
        id: xEffect
        source: xSound
    }

    MediaPlayer {
        id: oEffect
        source: oSound
    }

    SpriteSequence {
        id: x
        visible: false
        width: parent.width * ratio
        height: width
        anchors.centerIn: parent
        Sprite {
            name: "drawing"
            source: xImage
            frameCount: fCount
            frameWidth: fWidth
            frameHeight: fHeight
            frameRate: fSec
            to: {"done":1}
        }
        Sprite {
            name: "done"
            source: xImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: fLast * fWidth
        }
    }

    SpriteSequence {
        id: o
        visible: false
        width: parent.width * ratio
        height: width
        anchors.centerIn: parent
        Sprite {
            name: "drawing"
            source: oImage
            frameCount: fCount
            frameWidth: fWidth
            frameHeight: fHeight
            frameRate: fSec
            to: {"done":1}
        }
        Sprite {
            name: "done"
            source: oImage
            frameCount: 1
            frameWidth: fWidth
            frameHeight: fHeight
            frameX: fLast * fWidth
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(!game.over && !drawn) {
                if (oxboard.bits & game.playerX) {
                    drawX();
                    oxboard.xprior = (oxboard.bits & 0x1FF) | i;
                    oxboard.bits |= i;
                }
                else {
                    drawO();
                    oxboard.oprior = ((oxboard.bits >> 16) & 0x1FF) | i;
                    oxboard.bits |= i << 16;
                }
            }
        }
    }

}
